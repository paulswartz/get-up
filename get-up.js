'use strict';
var Steps = new Meteor.Collection("steps");

if (Meteor.isClient) {
  var syncInterval = 1000 * 60 * 15, intervalId;

  Template.display.minutes = function() {
    var steps = Steps.findOne().steps;
    return [
      {class: 'primary', label: 'steps', count: steps.steps},
      {class: 'success', label: 'very active', count: steps.veryActiveMinutes},
      {class: 'info', label: 'fairly active', count: steps.fairlyActiveMinutes},
      {class: 'warning', label: 'lightly active', count: steps.lightlyActiveMinutes},
      {class: 'danger', label: 'sedentary', count: steps.sedentaryMinutes}
    ];
  };

  Template.display.lastSync = function() {
    var lastSync = new Date(Steps.findOne().lastSync);
    return lastSync.toLocaleString();
  };

  Template.display.nextSync = function() {
    var nextSync = new Date(Steps.findOne().lastSync + syncInterval);
    return nextSync.toLocaleString();
  };
  
  Meteor.methods({
    sync: function() {
      //alert('syncing');
    }
  });

  Meteor.startup(function() {
    Meteor.subscribe('steps');
    if (!intervalId) {
      // every 15 minutes
      intervalId = Meteor.setInterval(function() {
        Meteor.call('sync');
      }, syncInterval);
    }
    Meteor.call('sync');
  });
}

if (Meteor.isServer) {
  Meteor.publish("steps", function() {
    return Steps.find({user: this.userId});
  });
    
  var FitbitAPI = new Fitbit();

  Meteor.methods({
    sync: function() {
      var today = new Date(),
          dateString = today.getFullYear() + '-' +
            ('0' + (today.getMonth() + 1)).slice(-2) + '-' +
            ('0' + today.getDate()).slice(-2),
          url = 'user/-/activities/date/' + dateString + '.json',
          userId = Meteor.userId(),
          result = FitbitAPI.get(url),
          lastSync = Date.parse(result.headers.date),
          steps = result.data.summary;
      console.log(result);
      Steps.update({user: userId}, {$set: {steps: steps, lastSync: lastSync}}, {upsert: true});
      return steps;
    }
  });
}
